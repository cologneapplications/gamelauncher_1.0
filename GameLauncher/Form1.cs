﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Web;

namespace GameLauncher {

    public partial class Form1 : Form {

        private string path_version = @"C:/test/version";
        private string path_tempVersion = @"C:/test/temp/version";
        private string path_temp = @"C:/test/temp/";
        private string path_decomp = @"C:/test/temp/decomp/";
        private string path_updateZIP = @"C:/test/temp/Update.zip";
        private string path_gameData = @"C:/test/game_data/";
        private string path_exec = @"C:/test/game_data/Eclipse.exe";

        public Form1() {
            InitializeComponent();
            Check();
        }
        private void Check() {
            WebClient client = new WebClient();
            client.DownloadFileAsync(new Uri("http://cologne-applications.de/test_files/version"), path_tempVersion);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(version_DownloadFileCompleted);
        }

        void version_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e) {
            if (File.Exists(path_version)) {
                if (File.ReadLines(path_version).First().ToString().Equals(File.ReadLines(path_tempVersion).First().ToString())) {
                    newestVersion();
                } else {
                    Download(File.ReadLines(path_tempVersion).First().ToString());
                }
            }else {
                WebClient client = new WebClient();
                if (!Directory.Exists(path_temp))
                    Directory.CreateDirectory(path_temp);
                client.DownloadFileAsync(new Uri("http://cologne-applications.de/test_files/version"), path_tempVersion);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(version_DownloadFileCompleted2);
            }
        }

        void version_DownloadFileCompleted2(object sender, AsyncCompletedEventArgs e) {
            Download(File.ReadLines(path_tempVersion).First().ToString());
        }

        private void Download(string version) {
            File.Copy(path_tempVersion, path_version, true);
            File.Delete(path_tempVersion);
            WebClient client = new WebClient();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
            client.DownloadFileAsync(new Uri("http://cologne-applications.de/test_files/Eclipse.zip"), @"C:\test\temp\Update.zip");
        }

        [Obsolete("Please use 'FileSystem.DeleteFolderContent(string path)'")]
        private void DeleteFiles() {
            System.IO.DirectoryInfo di = new DirectoryInfo(path_decomp);

            foreach (FileInfo file in di.GetFiles()) {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories()) {
                dir.Delete(true);
            }
        }

        private void Extract() {
            ZipFile.ExtractToDirectory(path_updateZIP, path_decomp);
            FileSystem.Copy(path_decomp, path_gameData, true);
            FileSystem.DeleteFolderContent(path_decomp);
            File.Delete(path_updateZIP);
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e) {
            int max = (int)e.TotalBytesToReceive / 1024 / 1024;
            int val = (int)e.BytesReceived / 1024 / 1024;
            double perc = (double)val / (double)max * 100;
            progressBar1.Maximum = max;
            progressBar1.Value = val;
            label1.Text = (val + "MB/" + max + "MB " + Math.Round(perc, 0) + "%");
            startButton.Text = "Updating";
        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e) {
            startButton.Enabled = true;
            label1.Text = label1.Text + "  Ready to Play!";
            startButton.Text = "Start";
            startButton.ForeColor = Color.Green;
            Extract();
        }

        private void newestVersion() {
            startButton.Enabled = true;
            label1.Text = "Ready to Play!";
            startButton.Text = "Start";
            startButton.ForeColor = Color.Green;
            progressBar1.Value = 100;
            File.Delete(path_tempVersion);
        }

        private void startButton_Click(object sender, EventArgs e) {
            Process.Start(path_exec);
        }

        [Obsolete(@"Please use 'DirectoryCopy.Copy(string source, string destination, bool copySubDirs)'!")]
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs) {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists) {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName)) {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files) {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs) {
                foreach (DirectoryInfo subdir in dirs) {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        void Form1_Load(object sender, EventArgs e) {

        }

        void webBrowser1_DocumentCompleted(object sender, EventArgs e) {

        }

        void progressBar1_Click(object sender, EventArgs e) {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            //  Process.Start("https://goo.gl/forms/mEI051vUNwsGwL4g2");
            linkLabel1.Text=RequestHelper.makeRequest("api/login", "POST", "username=test&&password=1234", "status");
        }

        public void setLinkLabel1(String s) {
            linkLabel1.Text = s;
        }
    }
}
