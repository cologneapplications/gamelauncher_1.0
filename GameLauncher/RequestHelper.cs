﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLauncher {
    class RequestHelper {
        public static string url = "http://www.cologne-applications.de/";
        public static string contentType = "application/x-www-form-urlencoded";

        public static string makeRequest(string endpoint, string method, string payload, string key) {
            System.Net.WebRequest webRequest = System.Net.WebRequest.Create(RequestHelper.url + endpoint);
            webRequest.ContentType = RequestHelper.contentType;
            webRequest.Method = method;
            System.IO.Stream stream = webRequest.GetRequestStream();
            stream.Write(Encoding.UTF8.GetBytes(payload), 0, payload.Length);
            stream.Close();
            System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)webRequest.GetResponse();
            Newtonsoft.Json.Linq.JObject o = Newtonsoft.Json.Linq.JObject.Parse(new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd());
            return o.Value<string>(key);
        }
    }
}
